package com.licorescalero.productsbc.controllers;

import com.licorescalero.productsbc.dtos.BaseResponseDTO;
import com.licorescalero.productsbc.dtos.categories.CategoryDTO;
import com.licorescalero.productsbc.entities.CategoryEntity;
import com.licorescalero.productsbc.services.CategoryService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("/api/categories")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ModelMapper modelMapper;

    @PostMapping
    public ResponseEntity<BaseResponseDTO<CategoryDTO>> create(@RequestBody CategoryDTO categoryDTO) {
        CategoryEntity categoryToCreate = modelMapper.map(categoryDTO, CategoryEntity.class);

        CategoryEntity categoryEntityCreated = categoryService.save(categoryToCreate);

        CategoryDTO categoryCreatedDTO = modelMapper.map(categoryEntityCreated, CategoryDTO.class);

        BaseResponseDTO<CategoryDTO> responseDTO = BaseResponseDTO.<CategoryDTO>builder()
                .data(categoryCreatedDTO)
                .message("Category created successfully")
                .build();

        return ResponseEntity.status(HttpStatus.CREATED).body(responseDTO);
    }

    @GetMapping
    public ResponseEntity<BaseResponseDTO<List<CategoryDTO>>> readAll() {
        Iterable<CategoryEntity> categoryEntities = categoryService.findAll();
        List<CategoryDTO> categoryDTOs = modelMapper.map(categoryEntities, new TypeToken<List<CategoryDTO>>() {}.getType());
        BaseResponseDTO<List<CategoryDTO>> responseDTO = BaseResponseDTO.<List<CategoryDTO>>builder()
                .data(categoryDTOs)
                .message("Categories successfully")
                .build();
        return ResponseEntity.ok(responseDTO);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponseDTO<Boolean>> deleteById(@PathVariable(value = "id") Long id) {
        categoryService.deleteById(id);

        BaseResponseDTO<Boolean> responseDTO = BaseResponseDTO.<Boolean>builder()
                .data(true)
                .message("Category deleted successfully")
                .build();

        return ResponseEntity.ok(responseDTO);
    }

    @PutMapping("/{id}")
    public ResponseEntity<BaseResponseDTO<CategoryDTO>> updateById (@PathVariable(value = "id") Long id, @RequestBody CategoryDTO categoryDTO) {
        CategoryEntity categoryToUpdate = modelMapper.map(categoryDTO, CategoryEntity.class);
        categoryToUpdate.setId(id);

        CategoryEntity categoryEntityCreated = categoryService.save(categoryToUpdate);

        CategoryDTO categoryUpdatedDTO = modelMapper.map(categoryEntityCreated, CategoryDTO.class);

        BaseResponseDTO<CategoryDTO> responseDTO = BaseResponseDTO.<CategoryDTO>builder()
                .data(categoryUpdatedDTO)
                .message("Category updated successfully")
                .build();

        return ResponseEntity.ok(responseDTO);
    }

}
