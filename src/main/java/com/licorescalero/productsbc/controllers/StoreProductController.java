package com.licorescalero.productsbc.controllers;

import com.licorescalero.productsbc.dtos.BaseResponseDTO;
import com.licorescalero.productsbc.dtos.store_products.StoreProductDTO;
import com.licorescalero.productsbc.entities.ProductEntity;
import com.licorescalero.productsbc.entities.StoreEntity;
import com.licorescalero.productsbc.entities.StoreProductEntity;
import com.licorescalero.productsbc.services.StoreProductService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.swing.text.html.Option;
import javax.websocket.server.PathParam;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/store-products")
public class StoreProductController {

    @Autowired
    private StoreProductService storeProductService;

    @Autowired
    private ModelMapper modelMapper;

    @PostMapping
    public ResponseEntity<BaseResponseDTO<StoreProductDTO>> create(@RequestBody StoreProductDTO storeProductDTO) {

        Optional<StoreProductEntity> storeProductEntityOptional = storeProductService.findByStoreIdAndProductId(
                storeProductDTO.getStoreId(),
                storeProductDTO.getProductId()
        );

        if(storeProductEntityOptional.isPresent()) {
            BaseResponseDTO<StoreProductDTO> responseDTO = BaseResponseDTO.<StoreProductDTO>builder()
                    .message("Error creating store_product")
                    .error("store_product with store_id " + storeProductDTO.getStoreId()
                            + " and product_id " + storeProductDTO.getProductId()
                            + " already exists"
                    )
                    .build();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseDTO);
        } else {
            StoreProductEntity storeProductToCreate = StoreProductEntity.builder()
                    .store(new StoreEntity(storeProductDTO.getStoreId()))
                    .product(new ProductEntity(storeProductDTO.getProductId()))
                    .quantity(storeProductDTO.getQuantity())
                    .price(storeProductDTO.getPrice())
                    .build();

            StoreProductEntity storeProductEntityCreated = storeProductService.save(storeProductToCreate);

            StoreProductDTO storeProductCreatedDTO = modelMapper.map(storeProductEntityCreated, StoreProductDTO.class);

            BaseResponseDTO<StoreProductDTO> responseDTO = BaseResponseDTO.<StoreProductDTO>builder()
                    .data(storeProductCreatedDTO)
                    .message("Store Product created successfully")
                    .build();

            return ResponseEntity.status(HttpStatus.CREATED).body(responseDTO);
        }
    }

    @GetMapping
    public ResponseEntity<BaseResponseDTO<List<StoreProductDTO>>> readAll(@RequestParam(value = "store_id", required = false) Long storeId, @RequestParam(value = "category_id", required = false) Long categoryId, @RequestParam(value = "product_id", required = false) Long productId) {
        Iterable<StoreProductEntity> storeProductEntities = storeProductService.findAll(storeId, categoryId, productId);
        List<StoreProductDTO> storeProductDTOs = modelMapper.map(storeProductEntities, new TypeToken<List<StoreProductDTO>>() {}.getType());
        BaseResponseDTO<List<StoreProductDTO>> responseDTO = BaseResponseDTO.<List<StoreProductDTO>>builder()
                .data(storeProductDTOs)
                .message("Store Products listed successfully")
                .build();
        return ResponseEntity.ok(responseDTO);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponseDTO<Boolean>> deleteById(@PathVariable(value = "id") Long id) {
        storeProductService.deleteById(id);

        BaseResponseDTO<Boolean> responseDTO = BaseResponseDTO.<Boolean>builder()
                .data(true)
                .message("Store Product deleted successfully")
                .build();

        return ResponseEntity.ok(responseDTO);
    }

    @PutMapping("/{id}")
    public ResponseEntity<BaseResponseDTO<StoreProductDTO>> updateById (@PathVariable(value = "id") Long id, @RequestBody StoreProductDTO storeProductDTO) {

        Optional<StoreProductEntity> storeProductEntityOptional = storeProductService.findById(id);

        if(storeProductEntityOptional.isPresent()) {
            StoreProductEntity storeProductToUpdate = storeProductEntityOptional.get();
            storeProductToUpdate.setPrice(storeProductDTO.getPrice());
            storeProductToUpdate.setQuantity(storeProductDTO.getQuantity());

            StoreProductEntity storeProductEntityCreated = storeProductService.save(storeProductToUpdate);

            StoreProductDTO storeProductUpdatedDTO = modelMapper.map(storeProductEntityCreated, StoreProductDTO.class);

            BaseResponseDTO<StoreProductDTO> responseDTO = BaseResponseDTO.<StoreProductDTO>builder()
                    .data(storeProductUpdatedDTO)
                    .message("Store Product updated successfully")
                    .build();

            return ResponseEntity.ok(responseDTO);
        } else {
            BaseResponseDTO<StoreProductDTO> responseDTO = BaseResponseDTO.<StoreProductDTO>builder()
                    .message("Error updating store_product")
                    .error("store_product with id " + id + " not found"
                    )
                    .build();
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseDTO);
        }
    }

    @PutMapping("/quantities")
    public ResponseEntity<BaseResponseDTO<List<StoreProductDTO>>> updateAll(@RequestBody List<StoreProductDTO> storeProductDTOList) {

        StringBuilder error = new StringBuilder();
        ArrayList<StoreProductDTO> response = new ArrayList<>();

        for(StoreProductDTO storeProductDTO: storeProductDTOList) {
            Optional<StoreProductEntity> storeProductEntityOptional = storeProductService.findById(storeProductDTO.getId());

            if(storeProductEntityOptional.isPresent()) {
                StoreProductEntity storeProductToUpdate = storeProductEntityOptional.get();
                storeProductToUpdate.setQuantity(storeProductToUpdate.getQuantity() - storeProductDTO.getQuantity());
                StoreProductEntity storeProductEntityCreated = storeProductService.save(storeProductToUpdate);
                StoreProductDTO storeProductUpdatedDTO = modelMapper.map(storeProductEntityCreated, StoreProductDTO.class);
                response.add(storeProductUpdatedDTO);
            } else {
                error.append("Store product with id ").append(storeProductDTO.getId()).append(" not found");
            }
        }

        BaseResponseDTO<List<StoreProductDTO>> responseDTO = BaseResponseDTO.<List<StoreProductDTO>>builder()
                .data(response)
                .message("Store Products updated successfully")
                .error(error.toString().trim().isEmpty() ? null : error.toString())
                .build();

        return ResponseEntity.ok(responseDTO);

    }

}
