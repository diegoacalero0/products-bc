package com.licorescalero.productsbc.controllers;

import com.licorescalero.productsbc.dtos.BaseResponseDTO;
import com.licorescalero.productsbc.dtos.products.ProductDTO;
import com.licorescalero.productsbc.dtos.store_products.StoreProductDTO;
import com.licorescalero.productsbc.entities.ProductEntity;
import com.licorescalero.productsbc.services.ProductService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @Autowired
    private ModelMapper modelMapper;

    @PostMapping
    public ResponseEntity<BaseResponseDTO<ProductDTO>> create(@RequestBody ProductDTO productDTO) {
        ProductEntity productEntityToCreate = modelMapper.map(productDTO, ProductEntity.class);
        ProductEntity productEntityCreated = productService.save(productEntityToCreate);

        ProductDTO productDTOCreated = modelMapper.map(productEntityCreated, ProductDTO.class);
        BaseResponseDTO<ProductDTO> responseDTO = BaseResponseDTO.<ProductDTO>builder()
                .data(productDTOCreated)
                .message("Product created successfully")
                .build();

        return ResponseEntity.status(HttpStatus.CREATED).body(responseDTO);
    }

    @GetMapping
    public ResponseEntity<BaseResponseDTO<List<ProductDTO>>> readAll(@RequestParam(value = "barcode", required = false) String barcode) {
        Iterable<ProductEntity> productEntities = productService.findAll(barcode);
        List<ProductDTO> productDTOs = modelMapper.map(productEntities, new TypeToken<List<ProductDTO>>() {
        }.getType());
        BaseResponseDTO<List<ProductDTO>> responseDTO = BaseResponseDTO.<List<ProductDTO>>builder()
                .data(productDTOs)
                .message("Products listed successfully")
                .build();
        return ResponseEntity.ok(responseDTO);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponseDTO<Boolean>> deleteById(@PathVariable(value = "id") Long id) {
        productService.deleteById(id);

        BaseResponseDTO<Boolean> responseDTO = BaseResponseDTO.<Boolean>builder()
                .data(true)
                .message("Product deleted successfully")
                .build();

        return ResponseEntity.ok(responseDTO);
    }

    @PutMapping("/{id}")
    public ResponseEntity<BaseResponseDTO<ProductDTO>> updateById (@PathVariable(value = "id") Long id, @RequestBody ProductDTO productDTO) {
        Optional<ProductEntity> productEntityOptional = productService.findById(id);

        if(productEntityOptional.isPresent()) {

            ProductEntity productEntityToUpdate = productEntityOptional.get();
            productEntityToUpdate.setName(productDTO.getName());

            ProductEntity productEntityUpdated = productService.save(productEntityToUpdate);
            ProductDTO productUpdatedDTO = modelMapper.map(productEntityUpdated, ProductDTO.class);

            BaseResponseDTO<ProductDTO> responseDTO = BaseResponseDTO.<ProductDTO>builder()
                    .data(productUpdatedDTO)
                    .message("Product updated successfully")
                    .build();

            return ResponseEntity.ok(responseDTO);
        } else {
            BaseResponseDTO<ProductDTO> responseDTO = BaseResponseDTO.<ProductDTO>builder()
                    .message("Error updating product")
                    .error("product with id " + id + " not found"
                    )
                    .build();
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseDTO);
        }
    }

}
