package com.licorescalero.productsbc.controllers;

import com.licorescalero.productsbc.dtos.BaseResponseDTO;
import com.licorescalero.productsbc.dtos.categories.CategoryDTO;
import com.licorescalero.productsbc.dtos.categoryproducts.CategoryProductDTO;
import com.licorescalero.productsbc.entities.CategoryEntity;
import com.licorescalero.productsbc.entities.CategoryProductEntity;
import com.licorescalero.productsbc.entities.ProductEntity;
import com.licorescalero.productsbc.services.CategoryProductService;
import com.licorescalero.productsbc.services.CategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/api/category-products")
public class CategoryProductController {

    @Autowired
    private CategoryProductService categoryProductService;

    @Autowired
    private ModelMapper modelMapper;

    @PostMapping
    public ResponseEntity<BaseResponseDTO<CategoryProductDTO>> create(@RequestBody CategoryProductDTO categoryProductDTO) {

        Optional<CategoryProductEntity> categoryProductEntityOptional = categoryProductService
                .findByCategoryIdAndProductId(categoryProductDTO.getCategoryId(), categoryProductDTO.getProductId());

        if(categoryProductEntityOptional.isPresent()) {
            BaseResponseDTO<CategoryProductDTO> responseDTO = BaseResponseDTO.<CategoryProductDTO>builder()
                    .message("Error creating category_product")
                    .error("category_product with category_id " + categoryProductDTO.getCategoryId()
                            + " and product_id " + categoryProductDTO.getProductId()
                            + " already exists"
                    )
                    .build();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseDTO);
        } else {
            CategoryProductEntity categoryProductEntityToCreate = CategoryProductEntity.builder()
                    .category(new CategoryEntity(categoryProductDTO.getCategoryId()))
                    .product(new ProductEntity(categoryProductDTO.getProductId()))
                    .build();

            CategoryProductEntity categoryProductEntityCreated = categoryProductService.save(categoryProductEntityToCreate);

            CategoryProductDTO categoryProductCreatedDTO = CategoryProductDTO.builder()
                    .id(categoryProductEntityCreated.getId())
                    .categoryId(categoryProductEntityCreated.getCategory().getId())
                    .productId(categoryProductEntityCreated.getProduct().getId())
                    .build();

            BaseResponseDTO<CategoryProductDTO> responseDTO = BaseResponseDTO.<CategoryProductDTO>builder()
                    .data(categoryProductCreatedDTO)
                    .message("Category product created successfully")
                    .build();

            return ResponseEntity.status(HttpStatus.CREATED).body(responseDTO);
        }
    }

}
