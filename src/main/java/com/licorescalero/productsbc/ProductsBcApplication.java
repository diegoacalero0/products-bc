package com.licorescalero.productsbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductsBcApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductsBcApplication.class, args);
	}
}
