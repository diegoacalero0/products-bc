package com.licorescalero.productsbc.repositories;

import com.licorescalero.productsbc.entities.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, Long> {

    @Query("SELECT pe FROM ProductEntity pe"
        + " WHERE (:barcode is null or barcode = :barcode)")
    List<ProductEntity> findAllByBarcode(String barcode);
}
