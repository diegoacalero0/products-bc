package com.licorescalero.productsbc.repositories;

import com.licorescalero.productsbc.entities.CategoryProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CategoryProductRepository extends JpaRepository<CategoryProductEntity, Long> {
    Optional<CategoryProductEntity> findByCategoryIdAndProductId(Long categoryId, Long productId);
}
