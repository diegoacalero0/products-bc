package com.licorescalero.productsbc.repositories;

import com.licorescalero.productsbc.entities.StoreProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StoreProductRepository extends JpaRepository<StoreProductEntity, Long> {

    @Query("SELECT distinct sp FROM StoreProductEntity sp"
            + " LEFT JOIN sp.product p"
            + " LEFT JOIN p.categoryProducts cp"
            + " WHERE (:storeId is null or sp.store.id = :storeId)"
            + " AND (:categoryId is null or cp.category.id = :categoryId)"
            + " AND (:productId is null or sp.product.id = :productId)"
    )
    Iterable<StoreProductEntity> findAllByStoreIdAndCategoryIdAndProductId(Long storeId, Long categoryId, Long productId);

    Optional<StoreProductEntity> findByStoreIdAndProductId(Long storeId, Long productId);
}
