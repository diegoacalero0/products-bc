package com.licorescalero.productsbc.services;

import com.licorescalero.productsbc.entities.CategoryEntity;

public interface CategoryService {
    public Iterable<CategoryEntity> findAll();
    public CategoryEntity save(CategoryEntity categoryEntity);
    public void deleteById(Long id);
}
