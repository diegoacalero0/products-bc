package com.licorescalero.productsbc.services;

import com.licorescalero.productsbc.entities.ProductEntity;

import java.util.Optional;

public interface ProductService {
    public Iterable<ProductEntity> findAll(String barcode);
    public ProductEntity save(ProductEntity productEntity);
    public void deleteById(Long id);
    public Optional<ProductEntity> findByBarcode(String barcode);
    public Optional<ProductEntity> findById(Long id);
}
