package com.licorescalero.productsbc.services;

import com.licorescalero.productsbc.entities.StoreProductEntity;

import java.util.Optional;

public interface StoreProductService {
    public Iterable<StoreProductEntity> findAll(Long storeId, Long categoryId, Long productId);
    public StoreProductEntity save(StoreProductEntity storeProductEntity);
    public void deleteById(Long id);
    public Optional<StoreProductEntity> findByStoreIdAndProductId(Long storeId, Long productId);
    public Optional<StoreProductEntity> findById(Long id);
}
