package com.licorescalero.productsbc.services.impl;

import com.licorescalero.productsbc.entities.StoreProductEntity;
import com.licorescalero.productsbc.repositories.StoreProductRepository;
import com.licorescalero.productsbc.services.StoreProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class StoreProductServiceImpl implements StoreProductService {

    @Autowired
    private StoreProductRepository storeProductRepository;

    @Override
    public StoreProductEntity save(StoreProductEntity storeProductEntity) {
        return storeProductRepository.save(storeProductEntity);
    }

    @Override
    public void deleteById(Long id) {
        storeProductRepository.deleteById(id);
    }

    @Override
    public Iterable<StoreProductEntity> findAll(Long storeId, Long categoryId, Long productId) {
        return storeProductRepository.findAllByStoreIdAndCategoryIdAndProductId(storeId, categoryId, productId);
    }

    @Override
    public Optional<StoreProductEntity> findByStoreIdAndProductId(Long storeId, Long productId) {
        return storeProductRepository.findByStoreIdAndProductId(storeId, productId);
    }

    @Override
    public Optional<StoreProductEntity> findById(Long id) {
        return storeProductRepository.findById(id);
    }
}
