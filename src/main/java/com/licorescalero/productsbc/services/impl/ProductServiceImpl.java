package com.licorescalero.productsbc.services.impl;

import com.licorescalero.productsbc.entities.ProductEntity;
import com.licorescalero.productsbc.repositories.ProductRepository;
import com.licorescalero.productsbc.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public Iterable<ProductEntity> findAll(String barcode) {
        return productRepository.findAllByBarcode(barcode);
    }

    @Override
    public ProductEntity save(ProductEntity productEntity) {
        return productRepository.save(productEntity);
    }

    @Override
    public void deleteById(Long id) {
        productRepository.deleteById(id);
    }

    @Override
    public Optional<ProductEntity> findByBarcode(String barcode) {
        return productRepository.findOne(Example.of(
                ProductEntity.builder()
                        .barcode(barcode)
                        .build()
        ));
    }

    @Override
    public Optional<ProductEntity> findById(Long id) {
        return productRepository.findById(id);
    }
}
