package com.licorescalero.productsbc.services.impl;

import com.licorescalero.productsbc.entities.CategoryProductEntity;
import com.licorescalero.productsbc.repositories.CategoryProductRepository;
import com.licorescalero.productsbc.services.CategoryProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CategoryProductServiceImpl implements CategoryProductService {

    @Autowired
    private CategoryProductRepository categoryProductRepository;

    @Override
    public CategoryProductEntity save(CategoryProductEntity categoryProductEntity) {
        return categoryProductRepository.save(categoryProductEntity);
    }

    @Override
    public void deleteById(Long id) {
        categoryProductRepository.deleteById(id);
    }

    @Override
    public Optional<CategoryProductEntity> findByCategoryIdAndProductId(Long categoryId, Long productId) {
        return categoryProductRepository.findByCategoryIdAndProductId(categoryId, productId);
    }


}
