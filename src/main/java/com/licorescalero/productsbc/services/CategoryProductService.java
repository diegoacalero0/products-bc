package com.licorescalero.productsbc.services;

import com.licorescalero.productsbc.entities.CategoryProductEntity;

import java.util.Optional;

public interface CategoryProductService {
    public CategoryProductEntity save(CategoryProductEntity categoryProductEntity);
    public void deleteById(Long id);
    public Optional<CategoryProductEntity> findByCategoryIdAndProductId(Long categoryId, Long productId);
}
