package com.licorescalero.productsbc.dtos.categoryproducts;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryProductDTO implements Serializable {

    private static final long serialVersionUID = -4597214957110968081L;

    private Long id;

    @JsonProperty(value = "category_id")
    private Long categoryId;

    @JsonProperty(value = "product_id")
    private Long productId;
}
