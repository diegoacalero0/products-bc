package com.licorescalero.productsbc.dtos.categories;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.licorescalero.productsbc.dtos.categoryproducts.CategoryProductDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryDTO implements Serializable {

    private static final long serialVersionUID = 1902205391907365095L;

    private Long id;

    @NotNull(message = "The field name name be null")
    private String name;
}
