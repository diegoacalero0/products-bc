package com.licorescalero.productsbc.dtos.store_products;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.licorescalero.productsbc.dtos.products.ProductDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StoreProductDTO implements Serializable {

    private static final long serialVersionUID = 596964217213925070L;

    private Long id;

    @JsonProperty(value = "store_id")
    @NotNull(message = "The field store_id cannot be null")
    private Long storeId;

    @JsonProperty(value = "product_id")
    @NotNull(message = "The field product_id cannot be null")
    private Long productId;

    @NotNull(message = "The field quantity cannot be null")
    private int quantity;

    @NotNull(message = "The field price cannot be null")
    private double price;

    private ProductDTO product;
}
