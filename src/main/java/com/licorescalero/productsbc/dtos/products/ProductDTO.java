package com.licorescalero.productsbc.dtos.products;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.licorescalero.productsbc.dtos.store_products.StoreProductDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDTO implements Serializable {

    private static final long serialVersionUID = 6464970828197308085L;

    private Long id;

    @NotNull(message = "The field name cannot be null")
    private String name;

    private String image;

    @NotNull(message = "The field barcode cannot be null")
    private String barcode;
}
