package com.licorescalero.productsbc.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@SuperBuilder(toBuilder = true)
@Getter
@Setter
@ToString
@Entity
@NoArgsConstructor
@Table(name = "categories")
public class CategoryEntity extends AbstractBaseEntity {

    private String name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "category")
    List<CategoryProductEntity> categoryProducts;

    public CategoryEntity(Long id) {
        super(id);
    }

}
