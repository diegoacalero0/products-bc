package com.licorescalero.productsbc.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@SuperBuilder(toBuilder = true)
@Getter
@Setter
@ToString
@Entity
@NoArgsConstructor
@Table(name = "category_products")
public class CategoryProductEntity extends AbstractBaseEntity {

    @ManyToOne
    @JoinColumn(name = "category_id", nullable = false)
    private CategoryEntity category;

    @ManyToOne
    @JoinColumn(name = "product_id", nullable = false)
    private ProductEntity product;

    public CategoryProductEntity(Long id) {
        super(id);
    }

}
