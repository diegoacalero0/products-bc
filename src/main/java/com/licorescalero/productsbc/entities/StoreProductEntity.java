package com.licorescalero.productsbc.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@SuperBuilder(toBuilder = true)
@Getter
@Setter
@ToString
@Entity
@NoArgsConstructor
@Table(name = "store_products")
public class StoreProductEntity extends AbstractBaseEntity {

    @ManyToOne
    @JoinColumn(name = "store_id", nullable = false)
    private StoreEntity store;

    @ManyToOne
    @JoinColumn(name = "product_id", nullable = false)
    private ProductEntity product;

    private int quantity;

    private double price;

    StoreProductEntity(Long id) {
        super(id);
    }

}
