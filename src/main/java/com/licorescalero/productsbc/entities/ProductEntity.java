package com.licorescalero.productsbc.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@SuperBuilder(toBuilder = true)
@Getter
@Setter
@ToString
@Entity
@NoArgsConstructor
@Table(name = "products")
public class ProductEntity extends AbstractBaseEntity {

    @Column(name = "name")
    private String name;

    private String image;

    private String barcode;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "product")
    private List<CategoryProductEntity> categoryProducts;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "product")
    private List<StoreProductEntity> storeProducts;

    public ProductEntity(Long id) {
        super(id);
    }

}
