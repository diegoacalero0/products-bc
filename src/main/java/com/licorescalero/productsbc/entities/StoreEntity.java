package com.licorescalero.productsbc.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@SuperBuilder(toBuilder = true)
@Getter
@Setter
@ToString
@Entity
@NoArgsConstructor
@Table(name = "stores")
public class StoreEntity extends AbstractBaseEntity {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "store")
    private List<StoreProductEntity> storeProducts;

    public StoreEntity(Long id) {
        super(id);
    }
}
